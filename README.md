Cinnamon Applet: Grabage Collector Version: v0.1
=================

Last version release date: 04 November 2014
***
Authors: [Lester Carballo Pérez](https://gitlab.com/lestcape).

Contact: lestcape@gmail.com
***

A Cinnamon Garbage Collector for automatic clean and notify the memory.


![](Capture.png)

If you have the same problem as shown in the image, where the RAM is completely consumed by the cinnamon precess and
this so upset as it's for me, probably you are thinking in go away from Cinnamon to another desktop. This is perhaps
what was done by other people in the [Forum of LinuxMint](http://forum.linuxmint.com/viewtopic.php?f=208&t=153281).
But please wait, because this may be a temporary solution for you until a complete solution can be implemented,
since it seems that the time to solve this issue could be really long.

You can see more about this problem here:
[CJS Issue about the GC](https://github.com/linuxmint/cjs/issues/15).

This tool can be configure to clean the memory (make a garbage collection) every certain amount of RAM consumed by the cinnamon process,
saving you to be forced to manually restart cinnamon periodically.

Change log
--------------

0.1- Beta
   - Initial release.

--------------
Installation Instructions:
--------------
1. Download this applet from their [website](https://gitlab.com/lestcape/Garbage-Collector) or directly if you want the
[master branch](https://gitlab.com/lestcape/Garbage-Collector/-/archive/master/Garbage-Collector-master.zip).

2. Unzip the downloaded file and copy the folder garbageCollector@lestcape at ~/.local/share/cinnamon/applets/

3. Enable the applet in Cinnamon Settings and use it.

Licence (this program is free software):
--------------
You can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see [GNU licenses](http://www.gnu.org/licenses).

Contribute
--------------

To report bugs, request new features and make suggestions, please visit the [Issues](https://gitlab.com/lestcape/Garbage-Collector/issues) session. 
You can also send us [Merge Requests](https://gitlab.com/lestcape/Garbage-Collector/merge_requests)

--------------

I hope you find it useful.

Lester.
