//Cinnamon Applet: Grabage Collector Version: v0.1
//Release Date: 04 November 2014
//
//Authors: Lester Carballo Pérez(https://github.com/lestcape).
//
//          Email: lestcape@gmail.com     Website: https://github.com/lestcape/Grabage-Collector
//
// "A Cinnamon Garbage Collector for automatic clean and notify the memory."
//

const St = imports.gi.St;

const Applet = imports.ui.applet;
const PopupMenu = imports.ui.popupMenu;
const Settings = imports.ui.settings;
const Tooltips = imports.ui.tooltips;
const Main = imports.ui.main;

const Lang = imports.lang;
const Mainloop = imports.mainloop;
const System = imports.system;

function MenuInformationMemoryItem(infoTitle, value, type) {
   this._init(infoTitle, value, type);
}

MenuInformationMemoryItem.prototype = {
   __proto__: PopupMenu.PopupBaseMenuItem.prototype,

   _init: function(infoTitle, value, type) {
      try {
         let params = { reactive: false, activate: false, hover: false,
                        sensitive: false, focusOnHover: false
                      }
         PopupMenu.PopupBaseMenuItem.prototype._init.call(this, params);
         this.titleLabel = new St.Label({ text: infoTitle });
         this.addActor(this.titleLabel);
         this.valueLabel = new St.Label({ text: " " + String(value) + " " });
         this.valueLabel.style = "font-family:Monospace";
         this.addActor(this.valueLabel);
         this.typeLabel = new St.Label({ text: type });
         this.addActor(this.typeLabel);    
      } catch(e) {
         global.logError(e);
      }
   },
    
   updateValue: function(value, type) {
      let fixedValue = String(value);
      if(Math.round(value) == value)
         fixedValue = fixedValue + ".00" ;
      this.valueLabel.set_text(" " + fixedValue + " ");
      this.typeLabel.set_text(type);
   }
};

function MeterBox(size) {
   this._init(size);
}

MeterBox.prototype = {
   _init: function(size) {
      this.size = size;
      this.override = false;
      this.actor = new St.BoxLayout({ vertical:false, style_class: 'meter-main-box'});
      this._buildColorPalete();
      this.boxesLed = new Array();
      let boxLed;
      for(let pos = 0; pos < this.size; pos++) {
         boxLed = new St.BoxLayout({ vertical:false, style_class: 'meter-led-box' });
         boxLed.style = "background-color: " + this.getDefaultColorInPalet() + ";";
         this.actor.add(boxLed, { x_fill: true, y_fill: true, expand: true, x_align: St.Align.START });
         this.boxesLed.push(boxLed);
      }
   },

   overrideTheme: function(override) {
      if(override) {
         for(let pos = 0; pos < this.size; pos++) {
            this.boxesLed[pos].set_style_class_name(' ');
         }
      } else {
         for(let pos = 0; pos < this.size; pos++) {
            this.boxesLed[pos].set_style_class_name('drives-meter-led');
         }
      }
   },

   setMeterImage: function(currentValue, totalValue) {
      let actualValue = 0;
      if(totalValue > 0) {
         actualValue = Math.floor(Math.round(this.size*currentValue/totalValue));
         let oldColorPos = 0;
         for(let pos = 0; pos < actualValue; pos++) {
            this.boxesLed[pos].style = "background-color: " + this.findColorInPalet(pos) + ";";
         }
      }
      for(let pos = actualValue; pos < this.size; pos++) {
         this.boxesLed[pos].style = "background-color: " + this.getDefaultColorInPalet() + ";";
      } 
   },

   findColorInPalet: function(posCurrent) {
      let valueColor = Math.floor(Math.round(100*posCurrent/this.size));
      for(let pos = 1; pos < this.colorPalete.length; pos++) {
         if(valueColor < this.colorPalete[pos][1])
            return this.colorPalete[pos][0];
      }
      return this.getDefaultColorInPalet();
   },

   getDefaultColorInPalet: function(currentValue) {
      return this.colorPalete[0][0];
   },

   _buildColorPalete: function(currentValue) {
      this.colorPalete = new Array();
      this.colorPalete.push(["#718397", 0]);
      this.colorPalete.push(["#009900", 20]);
      this.colorPalete.push(["#00cc00", 40]);
      this.colorPalete.push(["#00ff00", 60]);
      this.colorPalete.push(["#e4dd14", 80]);
      this.colorPalete.push(["#9a0030", 100]);
   }
};

function MyApplet(metadata, orientation, panel_height, instanceId) {
   this._init(metadata, orientation, panel_height, instanceId);
}

MyApplet.prototype = {
   __proto__: Applet.Applet.prototype,
    
   _init: function(metadata, orientation, panel_height, instanceId) {
      try {
         this.uuid = metadata["uuid"];
         this.orientation = orientation;
         Applet.Applet.prototype._init.call(this, this.orientation, panel_height);
         this.set_applet_tooltip("Grabage Collector");

         this.refresh = false;
         this.refresh_id = -1;
         this.initMemory = -1;
         this.lastMemory = -1;
         this._textSize = 8
         this.actor.set_vertical(true);
         this.title = new St.Label({ text: "0%" });
         this.actor.add(this.title, { x_fill: true, y_fill: false, expand: true, x_align: St.Align.MIDDLE, y_align: St.Align.END });
         this.title.style = "font-size: " + this._textSize + "pt; font-weight: bold;";
         this.meter = new MeterBox(10);
         this.actor.add(this.meter.actor, { x_fill: true, y_fill: false,expand: true, x_align: St.Align.MIDDLE, y_align: St.Align.START });
         this.meter.actor.set_height(panel_height/4);
         this.meter.actor.set_width(34);

         this.settings = new Settings.AppletSettings(this, this.uuid, instanceId);
         this.settings.bindProperty(Settings.BindingDirection.IN, "automatic-memory-clean", "automatic_memory_clean", this._on_alarm_setup);
         this.settings.bindProperty(Settings.BindingDirection.IN, "max-memory-allow", "max_memory_allow", this._on_alarm_setup);

         this.menuManager = new PopupMenu.PopupMenuManager(this);
         this.menu = new Applet.AppletPopupMenu(this, this.orientation);
         this.menuManager.addMenu(this.menu);

         this._build_menu();
         this.refresh_id = Mainloop.timeout_add_seconds(1, Lang.bind(this, this._on_alarm_setup));
      } catch(e) {
         Main.notify("error", e.message);
         global.logError(e);
      }
   },

   on_applet_removed_from_panel: function() {
      if(this.refresh_id > 0)
         Mainloop.source_remove(this.refresh_id);
   },

   _on_alarm_setup: function() {
      try {
         let info = this.get_memory_info()
         let totalMemory = info[2];
         if((this.refresh)||(this.lastMemory < 0)) {
            this.refresh = false;
            this.lastMemory = totalMemory;
         }
         if(this.initMemory < 0)
            this.initMemory = totalMemory;
         let memoryIncrease = Math.round((totalMemory - this.lastMemory)/1024.0/1024.0);
         if(memoryIncrease < 0) {
            this.lastMemory = totalMemory;
            memoryIncrease = 0;
         }
         if(memoryIncrease > this.max_memory_allow) {
            if(this.automatic_memory_clean)
               this.clean_gc();
            memoryIncrease = this.max_memory_allow;
         }
         let percent = Math.round(100*memoryIncrease/this.max_memory_allow);
         this.title.set_text(percent + "%")
         this.meter.setMeterImage(memoryIncrease, this.max_memory_allow);
         this.set_menu_values(info);
         if(this.refresh_id > 0)
            Mainloop.source_remove(this.refresh_id);
         this.refresh_id = Mainloop.timeout_add_seconds(1, Lang.bind(this, this._on_alarm_setup));
      } catch(e) {
         Main.notify("error", e.message);
         global.logError(e);
      }
   },

   clean_gc: function() {
      System.gc();
      this.refresh = true;
   },

   get_memory_info: function() {
      let memInfo = global.get_memory_info();
      let totalMemory = memInfo.glibc_uordblks
      totalMemory += memInfo.js_bytes
      totalMemory += memInfo.gjs_boxed
      totalMemory += memInfo.gjs_gobject
      totalMemory += memInfo.gjs_function
      totalMemory += memInfo.gjs_closure
      let result = [
         true,
         memInfo.last_gc_seconds_ago,
         totalMemory,
         {
            'glibc_uordblks': (memInfo.glibc_uordblks),
            'js_bytes': (memInfo.js_bytes),
            'gjs_boxed': (memInfo.gjs_boxed),
            'gjs_gobject': (memInfo.gjs_gobject),
            'gjs_function': (memInfo.gjs_function),
            'gjs_closure': (memInfo.gjs_closure)
         }
      ]
      return result;
   },
    
   on_applet_clicked: function(event) {
      try {
         this.menu.toggle();  
      } catch(e) {
         global.logError(e);
      }
   },
    
   _build_menu: function() {
      this.lostedMemory = new MenuInformationMemoryItem(_("cjs_lost"), 0, "B");
      this.menu.addMenuItem(this.lostedMemory);
      this.js_bytesItem = new MenuInformationMemoryItem("js_bytes", 0, "B");
      this.menu.addMenuItem(this.js_bytesItem);
      this.gjs_gobjectItem = new MenuInformationMemoryItem("gjs_gobject", 0, "B");
      this.menu.addMenuItem(this.gjs_gobjectItem);
      this.gjs_boxedItem = new MenuInformationMemoryItem("gjs_boxed", 0, "B");
      this.menu.addMenuItem(this.gjs_boxedItem);
      this.gjs_functionItem = new MenuInformationMemoryItem("gjs_function", 0, "B");
      this.menu.addMenuItem(this.gjs_functionItem);
      this.glibc_uordblksItem = new MenuInformationMemoryItem("glibc_uordblks", 0, "B");
      this.menu.addMenuItem(this.glibc_uordblksItem);

      this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
      let cleanGC = new Applet.MenuItem(_("Clean G.C."), "user-trash", Lang.bind(null, this.clean_gc));
      this.menu.addMenuItem(cleanGC);
   },

   set_menu_values: function(info) {
      let currentLost = this.lastMemory - this.initMemory;
      if(currentLost < 0)
         currentLost = 0;
      [value, type] = this._get_memory_format(currentLost);
      this.lostedMemory.updateValue(value, type);
      [value, type] = this._get_memory_format(info[3]["js_bytes"]);
      this.js_bytesItem.updateValue(value, type);
      [value, type] = this._get_memory_format(info[3]["gjs_gobject"]);
      this.gjs_gobjectItem.updateValue(value, type);
      [value, type] = this._get_memory_format(info[3]["gjs_boxed"]);
      this.gjs_boxedItem.updateValue(value, type);
      [value, type] = this._get_memory_format(info[3]["gjs_function"]);
      this.gjs_functionItem.updateValue(value, type);
      [value, type] = this._get_memory_format(info[3]["glibc_uordblks"]);
      this.glibc_uordblksItem.updateValue(value, type);
   },

   _get_memory_format: function(value) {
      if(value < 1000)
         return [Math.round(value*100)/100, "B"];
      if(value < 1000000)
         return [Math.round(value*100/1024.0)/100, "KB"];
      if(value < 1000000000)
         return [Math.round(value*100/1024.0/1024.0)/100, "MB"];
      return [Math.round(value*100/1024.0/1024.0/1024.0)/100, "GB"];
   }
};

function main(metadata, orientation, panel_height, instanceId) {
   let myApplet = new MyApplet(metadata, orientation, panel_height, instanceId);
   return myApplet;
}
